Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pvpgn
Source: https://github.com/pvpgn/pvpgn-server
Comment:
 This package was debianized by Robert Millan <rmh@debian.org>
 on 2003-12-28 19:06:22 +0100 Sun.
 .
 It was downloaded from:
    http://developer.berlios.de/projects/pvpgn
    http://pvpgn.org/
 .
 The zlib code is not used in the debian package of pvpgn.
Files-Excluded:
    scripts/localize/*.exe
    src/win32/resource.h
    src/win32/resource.rc
    files/*.bni
    files/*.mpq
    files/*.save
    files/*.smk

Files: *
Copyright: 2004-2010  The Bnetd project and others, see file CREDITS.
           2003-2004  Aaron
           2000       Alexey Belyaev <spider@omskart.ru>
           2002       Bart.omiej Butyn <bartek@milc.com.pl>
           2005       Bryan Biedenkapp <gatekeep@gmail.com>
           2004-2005  CreepLord <creeplord@pvpgn.org>
           1999       Descolada <dyn1-tnt9-237.chicago.il.ameritech.net>
           2000-2005  Dizzy
           1999-2000  D.Moreaux <vapula@linuxbe.org>
           2004       Donny Redmond <dredmond@linuxmail.org>
           2001-2002  Erik Latoshek [forester] <laterk@inbox.lv>
           2001       faster <lqx@cic.tsinghua.edu.cn>
           1999-2000  Gediminas <gediminas_lt@mailexcite.com>
           2001-2002  Gianluigi Tiesi <sherpya@netfarm.it>
           2001       Hakan Tandogan <hakan@gurkensalat.com>
           1999       Karl Fogel <kfogel@red-bean.com>
           2004       Lector
           1991-1992  Linus Torvalds
           2004       ls_sons <ls@gamelife.org>
           1999-2001  Marco Ziech <mmz@gmx.net>
           1998-1999  Mark Baysinger <mbaysing@ucsd.edu>
           2004-2005  Olaf Freyer <aaron@cs.tu-berlin.de>
           1999       Oleg Drokin <green@ccssu.ccssu.crimea.ua>
           2000-2001  Onlyer<(onlyer@263.net>
           2000       Otto Chan <kenshin_@hotmail.com>
           2006-2008  Pelish <pelish@gmail.com>
           1999       Philippe Dubois <pdubois1@hotmail.com>
           1999-2000  Rob Crittenden <rcrit@greyoak.com>
           2001       Roland Haeder <webmaster@ai-project-now.de>
           1998-2002  Ross Combs <ross@bnetd.org>
                                 <rocombs@cs.nmsu.edu>
           2001       sousou <liupeng.cs@263.net>
           2002       TheUndying
           2002       zap-zero
           2002       Zzzoom
           1987-2001  Free Software Foundation, Inc.
License: GPL-2+

Files: scripts/pvpgn_hash.inc.php
Copyright: 2004      Aaron <aaron@pvpgn.org>
           2002-2003 Marcus Campbell
License: LGPL
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License.
 .
 The complete text of the GNU Lesser General Public License
 can be found in "/usr/share/common-licenses/LGPL".

Files: src/tinycdb/*
Copyright: not-applicable
License: public-domain
 Public domain.

Files: src/zlib/*
Copyright: 1995-2002 Mark Adler <madler@alumni.caltech.edu>
           1995-2002 Jean-loup Gailly <jloup@gzip.org>
License:
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: src/compat/inet_aton.*
Copyright: 1983,1990,1993 The Regents of the University of California.  All rights reserved.
License:
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. All advertising materials mentioning features or use of this software
    must display the following acknowledgement:
      This product includes software developed by the University of
      California, Berkeley and its contributors.
 4. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
 -
 Portions Copyright (c) 1993 by Digital Equipment Corporation.
 .
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies, and that
 the name of Digital Equipment Corporation not be used in advertising or
 publicity pertaining to distribution of the document or software without
 specific, written prior permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND DIGITAL EQUIPMENT CORP. DISCLAIMS ALL
 WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS.   IN NO EVENT SHALL DIGITAL EQUIPMENT
 CORPORATION BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
 ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 SOFTWARE.
 -

Files: src/autoconf/install-sh
Copyright: 1991 the Massachusetts Institute of Technology
License:
 Permission to use, copy, modify, distribute, and sell this software and its
 documentation for any purpose is hereby granted without fee, provided that
 the above copyright notice appear in all copies and that both that
 copyright notice and this permission notice appear in supporting
 documentation, and that the name of M.I.T. not be used in advertising or
 publicity pertaining to distribution of the software without specific,
 written prior permission.  M.I.T. makes no representations about the
 suitability of this software for any purpose.  It is provided "as is"
 without express or implied warranty.

Files: debian/*
Copyright: 2003-2004 Robert Millan <rmh@debian.org>
           2005      Philipp Kern <pkern@debian.org>
           2005-2008 Radu Spineanu <radu@debian.org>
                                   <radu@timisoara.roedu.net>
           2010      Jari Aalto <jari.aalto@cante.net>
           2013-2021 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 The complete text of the GNU General Public License version 2
 can be found in "/usr/share/common-licenses/GPL-2".
